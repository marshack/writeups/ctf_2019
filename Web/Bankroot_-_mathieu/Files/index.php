<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

  <head> 
    <style>
      img {
        width: 50%;
        height: 50%;
        margin-left: auto;
        margin-right: auto;

      }
  </style>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bankroot</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/scrolling-nav.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Bankroot</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Accueil</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="bg-primary text-white">
      <?php
      if(isset($_SESSION['pseudo'])){
        ?>
          <div class="container text-center">
            <h1>Mon Compte</h1>
            <p class="lead">Compte personel de <?php echo $_SESSION['pseudo'] ?></p>
          </div>
        <?php
      }
      else{
        ?>
        <div class="container text-center">
          <h1>Bankroot</h1>
          <p class="lead">Veuillez vous connecter</p>
        </div>
        <?php
      }
      ?>
      
    </header>

    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <?php
            if(isset($_SESSION['pseudo'])){
              ?>
                <div class="container text-center">
                  <h1>Votre solde est de <code><?php echo $_SESSION['argent']; ?></code>BRT (Bankroot)</h1>
                  <?php
                  if($_SESSION['argent'] >= 1000000){
                    echo '<h1> Bien joué, le flag est MARS{D0LLAR$Baby}</h1>';
                  }
                  ?>
                  <p class="lead">Transaction</p>
                  <form action="transac.php" method="post">
                  <div class="row">
                    <div class="col">
                      <input type="int" class="form-control" placeholder="Somme" name="amount" id="somme">
                    </div>
                    <div class="col">
                      <input type="text" class="form-control" placeholder="ID compte" name="id">
                    </div>
                    <div class="col">
                      <input type="submit" class="btn btn-success" placeholder="ID compte">
                    </div>
                  </div>
                </form>
                </div>
              <?php
            }
            else{
              ?>
              <form class="form-signin" action="login.php" method="POST">
                <label for="inputEmail" class="sr-only">Pseudo</label>
                <input type="text" id="inputEmail" class="form-control" placeholder="Pseudo" required autofocus name="pseudo">
                <label for="inputPassword" class="sr-only">Mot-de-passe</label>
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
              </form>
              <?php
              if($_GET['err'] == 1){
                echo 'Mauvais mot de passe';
              }
            }
            ?>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; MARS@HACK 2019</p>
      </div>
      <!-- /.container -->
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom JavaScript for this theme -->
    <script src="js/scrolling-nav.js"></script>
     <script>
      $( "#somme" ).change(function() {
        if($("#somme").val() < 0){
          alert("La somme ne peut être négative !");
          $("#somme").val(0);
      }
      });
    </script>
  </body>

</html>
