CREATE DATABASE IF NOT EXISTS `chall` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `chall` ;

CREATE TABLE `articles` (
  `id` INT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `content` TEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `my_super_strange_table_of_things` (
  `id` INT NOT NULL,
  `login` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE USER 'chall'@'localhost' IDENTIFIED BY 'chall';

GRANT ALL PRIVILEGES ON `chall`.`articles` TO `chall`;
GRANT ALL PRIVILEGES ON `chall`.`my_super_strange_table_of_things` TO `chall`;

INSERT INTO `chall`.`articles` (`id`, `name`, `content`) VALUES (0, 'Article 1', 'Lorem Ipsum dolore sanctis sum est possum'), (1, 'Article 2', 'Lorem Ipsum dolore sanctis sum est possum'), (2, 'Article 3', 'Lorem Ipsum dolore sanctis sum est possum');

INSERT INTO `chall`.`my_super_strange_table_of_things` (`id`, `login`, `password`) VALUES (DEFAULT, 'admin', 'MARS{Uni0nIs4nAw353m3Op710n}');
