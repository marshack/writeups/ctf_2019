<?php
session_start();

if(!isset($_SESSION['id']))
   header('Location: connexion.php');
?>
<!DOCTYPE html>
<html>
   <head>
      <title>Edit profile</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">

      <!-- Bootstrap core CSS -->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

      <!-- Custom styles for this template -->
      <link href="css/scrolling-nav.css" rel="stylesheet">
   </head>
<?php

$bdd = new PDO('mysql:host=localhost;dbname=chall', 'chall', 'chall');
if(isset($_SESSION['id'])) 
{
   $token = 0;
   $requser = $bdd->prepare("SELECT * FROM users WHERE id = ?");
   $requser->execute(array($_SESSION['id']));
   $user = $requser->fetch();
   if(isset($_POST['newpseudo']) AND !empty($_POST['newpseudo']) AND $_POST['newpseudo'] != $user['login']) 
   {
      $newpseudo = htmlspecialchars($_POST['newpseudo']);
      $insertpseudo = $bdd->prepare("UPDATE users SET login = ? WHERE id = ?");
      $insertpseudo->execute(array($newpseudo, $_SESSION['id']));
      $token = 1;
   }
   if(isset($_POST['newmail']) AND !empty($_POST['newmail']) AND $_POST['newmail'] != $user['email']) 
   {
      $newmail = htmlspecialchars($_POST['newmail']);
      $insertmail = $bdd->prepare("UPDATE users SET email = ? WHERE id = ?");
      $insertmail->execute(array($newmail, $_SESSION['id']));
      $token = 1;
   }
   if(isset($_POST['newmdp1']) AND !empty($_POST['newmdp1']) AND isset($_POST['newmdp2']) AND !empty($_POST['newmdp2'])) 
   {
      $mdp1 = sha1($_POST['newmdp1']);
      $mdp2 = sha1($_POST['newmdp2']);
      if($mdp1 == $mdp2) 
      {
         $insertmdp = $bdd->prepare("UPDATE users SET password = ? WHERE id = ?");
         $insertmdp->execute(array($mdp1, $_SESSION['id']));
         $token = 1;
      } 
      else 
         $msg = "Vos deux mdp ne correspondent pas !";
   }
   if(isset($_POST['sexe']) AND !empty($_POST['sexe'])) 
   {
      $insertmdp = $bdd->prepare("UPDATE users SET sexe = ? WHERE id = ?");
      $insertmdp->execute(array($_POST['sexe'], $_SESSION['id']));
      $token = 1;
   }
   if(isset($_POST['admin']) AND !empty($_POST['admin'])) 
   {
      if(is_numeric($_POST['admin']))
      {
         $insertmdp = $bdd->prepare("UPDATE users SET admin = ? WHERE id = ?");
         $insertmdp->execute(array($_POST['admin'], $_SESSION['id']));
         $token = 1;
      } 
      else 
         $msg = "La variable admin doit être un nombre entier !";
   }
   if ($token == 1)
      header('Location: profile.php');
}
?>
   <body>
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
         <div class="container">
           <a class="navbar-brand js-scroll-trigger" href="#page-top">Esioc</a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon"></span>
           </button>
           <div class="collapse navbar-collapse" id="navbarResponsive">
             <ul class="navbar-nav ml-auto">
               <li class="nav-item">
                 <a class="nav-link" href="index.php">Accueil</a>
               </li>
               <?php
                  if(!isset($_SESSION['id']))
                     echo '<li class="nav-item"><a class="nav-link" href="inscription.php">Inscription</a></li><li class="nav-item"><a class="nav-link" href="connexion.php">Se connecter</a></li>'
               ?>
               <?php
                  if(isset($_SESSION['id']))
                     echo '<li class="nav-item"><a class="nav-link" href="deconnexion.php">Se déconnecter</a></li><li class="nav-item"><a class="nav-link" href="profile.php">Mon profile</a></li><li class="nav-item"><a class="nav-link" href="admin.php">Administration</a></li>';
               ?>
             </ul>
           </div>
         </div>
      </nav>

      <header class="bg-primary text-white">
         <div class="container text-center">
           <h1>ESIOC 62.430</h1>
           <p class="lead">Escadron des Systèmes d'Information Opérationnels et de Cyberdefense</p>
         </div>
      </header>

      <section id="about">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 mx-auto">
                  <div align="center">
                     <h2>Edition de mon profil</h2></br></br>
                     <div align="left">
                        <form method="POST" action="" enctype="multipart/form-data">
                           <label>Pseudo :</label>
                           <input type="text" name="newpseudo" placeholder="Pseudo" value="<?php echo $user['login']; ?>"/><br/><br/>
                           <label>Mail :</label>
                           <input type="text" name="newmail" placeholder="Mail" value="<?php echo $user['email']; ?>"/><br/><br/>
                           <label>Mot de passe :</label>
                           <input type="password" name="newmdp1" placeholder="Mot de passe"/><br/><br/>
                           <label>Confirmation - mot de passe :</label>
                           <input type="password" name="newmdp2" placeholder="Confirmation du mot de passe" /><br/><br/>
                           <label>Sexe :</label>
                           <select size="1" name="sexe">
                              <option value="Homme">Homme</option>
                              <option value="Femme">Femme</option>
                              <option value="Pas de valeur" selected>Pas de valeur</option>
                           </select></br></br>
                           <input class="btn btn-primary" type="submit" value="Mettre à jour mon profil !" />
                        </form>
                        <?php if(isset($msg)) { echo $msg; } ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
   <!-- Footer -->
   <footer class="py-5 bg-dark">
       <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; MARS@HACK 2019</p>
       </div>
   <!-- /.container -->
   </footer>
   <!-- Bootstrap core JavaScript -->
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   <!-- Plugin JavaScript -->
   <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
   <!-- Custom JavaScript for this theme -->
   <script src="js/scrolling-nav.js"></script>
</html>