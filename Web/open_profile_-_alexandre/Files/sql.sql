CREATE DATABASE IF NOT EXISTS `chall` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `chall` ;

CREATE TABLE `users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `sexe` VARCHAR(255),
  `admin` INT NOT NULL,
  `email` VARCHAR(255),
  PRIMARY KEY (`id`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE USER 'chall'@'localhost' IDENTIFIED BY 'chall';

GRANT ALL PRIVILEGES ON `chall`.`users` TO `chall`;

INSERT INTO `chall`.`users` (`id`, `login`, `password`, `sexe`, `admin`, `email`) VALUES (DEFAULT, 'admin', '9ede35bf42d4498611715f6fe26200b935521f5e', 'homme', '1', 'admin@admin.gouv.fr');
