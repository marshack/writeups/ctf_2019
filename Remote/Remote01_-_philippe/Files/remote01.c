// fichier source :  remote01.c
// gcc -m64 -o remote01 remote01.c


#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>


int main()
{
        printf("strcmp(user_input,flag) ?\n");
        fflush(stdout);
        int ret;
        int longueur;
        char *flag="MARS{D0nt_s3nd_st@tus_strcmp}";
                         
        char user_input[50];
       
        time_t origine = time(NULL);
        time_t stop;   
 

        while(true) {
           fgets(user_input,45,stdin);
           char *pos;
           if ((pos=strchr(user_input, '\n')) != NULL)
               *pos = '\0'; 
                      
           
           stop = time(NULL);
           if ((stop-origine) >= 10)
              { 
              printf("Too late !!!!\n");
              break;
              }
 
           ret = strcmp(user_input,flag);
           if (ret == 0){
             printf("Congratulations, The flag is %s\n\n",flag);
             fflush(stdout);
             //printf("\x00");
             //fflush(stdout);
             break;
             } 
           if (ret > 0)
             {
             printf("Status : %x\n",ret);
             fflush(stdout);
             }
           if (ret < 0)
             {
             printf("Status : %x\n",ret);
             fflush(stdout);
             }
           } 

}
