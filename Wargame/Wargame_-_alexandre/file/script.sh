#!/bin/bash

#Run all python binary

/root/challenge2/server.py &
/root/challenge6/server.py &
/root/challenge5/run.sh &
chmod 7701 /home/challenge4/nmap
chmod 7701 /home/challenge5/getflag
chmod 703 /tmp
chmod 760 /home/challenge9/.passwd
chown challenge10 /home/challenge9/.passwd
chmod 770 /home/challenge*
chown challenge0 /home/challenge0
chown challenge1 /home/challenge1
chown challenge2 /home/challenge2
chown challenge3 /home/challenge3
chown challenge4 /home/challenge4
chown challenge5 /home/challenge5
chown challenge6 /home/challenge6
chown challenge7 /home/challenge7
chown challenge8 /home/challenge8
chown challenge9 /home/challenge9
chown challenge10 /home/challenge10
/usr/sbin/sshd -D