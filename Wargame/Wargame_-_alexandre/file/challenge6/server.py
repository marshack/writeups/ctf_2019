#!/usr/bin/python2.7
# coding: utf-8 

import socket
import threading

class ClientThread(threading.Thread):

    def __init__(self, ip, port, clientsocket):

        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.clientsocket = clientsocket
        print("[+] Nouveau thread pour %s %s" % (self.ip, self.port, ))

    def run(self): 
   
        print("Connection de %s %s" % (self.ip, self.port, ))

        while True:
            r = self.clientsocket.recv(2048)
            print("Le password est: ", r, "...")
            data = r.strip()
            passw = open('/etc/wargames_pass/level6/level6_password', 'r').read()
            passw = passw.strip()
            data = data.split(" ")
            password = open('/etc/wargames_pass/level7/level7_password', 'r').read()
            if (data[0] == passw and data[1] == "6942"):
                response = "Félicitation, le password de l'utilisateurs suivant est : " + password + "\n"
                self.clientsocket.send(response)
            else:
                self.clientsocket.send("Raté, ce n'est pas le mot de passe précédent !\n")

tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind(("",9999))

while True:
    tcpsock.listen(10)
    print( "En écoute...")
    (clientsocket, (ip, port)) = tcpsock.accept()
    newthread = ClientThread(ip, port, clientsocket)
    newthread.start()