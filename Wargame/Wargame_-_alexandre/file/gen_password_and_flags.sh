#!/bin/bash

filecount=0
while [ $filecount -lt 10 ]
do
prefix="3771_"
suffix="_3771"
flag=$prefix
randstring=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
flag+=$randstring
flag+=$suffix
if [ $filecount = 0 ]
then
	echo $flag > ./challenge${filecount}/-.passwd
elif [ $filecount = 3 ]
then
	echo "Nop"
else
	echo $flag > ./challenge${filecount}/.passwd
fi
((filecount++))
done
