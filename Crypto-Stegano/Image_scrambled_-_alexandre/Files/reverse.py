#!/usr/bin/python3

from __future__ import print_function
import os
from sys import argv
from PIL import Image
import random
from collections import defaultdict

tab = {}
if len(argv) == 2:
  img = Image.open(argv[1])
  x, y = img.size
  i = 0
  a = 0
  n = x/135
  c = x/135
  while i != 135:
    tab[i] = img.crop((a, 0, c, y))
    i += 1
    a = c
    c += n
  seed = "MARS@HACK"
  i = 0
  num_seed = {}
  for i in range(len(seed)):
    num_seed[i] = ord(seed[i])
  i = 0
  passe = 0
  tampon_num = 0
  order = defaultdict(int)
  index = 0
  while(passe != len(seed)):
    while(i < len(num_seed)):
      if(tampon_num == 0 and order[i] == 0):
        tampon_num = num_seed[i]
        index = i
      elif(num_seed[i] < tampon_num and order[i] == 0 and i != 4):
        tampon_num = num_seed[i]
        index = i
      i += 1
    order[index] = passe
    tampon_num = 100
    i = 0
    passe = passe + 1
  passe = 0
  m = {}
  count = 0
  while(passe < 15):
    count = 9 * passe
    index = 0
    coeff = (passe + 1) * 9
    while(count < coeff):
      place = (order[index]) + (passe * len(order))
      m[count] = tab[place]
      index += 1
      count += 1
    index = 0
    passe += 1
  scrambled_image = Image.new('RGB', (x,y))
  x_offset = 0
  i = 0
  for i in range(len(m)):
    scrambled_image.paste(m[i], (x_offset, 0))
    x_offset += m[i].size[0]
  scrambled_image.save('flag.png')
else:
  print("Usage: ./test.py exemple.png")