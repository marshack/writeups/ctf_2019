#!/usr/bin/python3

from __future__ import print_function
import os
from sys import argv
from PIL import Image
import random
from collections import defaultdict

r = {}
if len(argv) == 2:
  b = Image.open(argv[1])
  x, y = b.size
  i = 0
  a = 0
  n = x/135
  c = x/135
  while i != 135:
    r[i] = b.crop((a, 0, c, y))
    i += 1
    a = c
    c += n
  p = "MARS@HACK"
  i = 0
  u = {}
  for i in range(len(p)):
    u[i] = ord(p[i])
  i = 0
  j = 0
  h = 0
  q = defaultdict(int)
  w = 0
  while(j != len(p)):
    while(i < len(u)):
      if(h == 0 and q[i] == 0):
        h = u[i]
        w = i
      elif(u[i] < h and q[i] == 0 and i != 4):
        h = u[i]
        w = i
      i += 1
    q[w] = j
    h = 100
    i = 0
    j = j + 1
  j = 0
  m = {}
  s = 0
  while(j < 15):
    s = 9 * j
    w = 0
    k = (j + 1) * 9
    while(s < k):
      v = (q[w]) + (j * len(q))
      m[v] = r[s]
      w += 1
      s += 1
    w = 0
    j += 1
  e = Image.new('RGB', (x,y))
  x_offset = 0
  i = 0
  for i in range(len(m)):
    e.paste(m[i], (x_offset, 0))
    x_offset += m[i].size[0]
  e.save('flag.png')
else:
  print("Usage: ./test.py exemple.png")