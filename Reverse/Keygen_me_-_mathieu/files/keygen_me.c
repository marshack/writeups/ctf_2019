#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int auth(char *user, char *key){
	int acc;
	char chaine[] = "TSU1N6MYOBSYF5";
	char len = strlen(user);
	chaine[0] = len + '0';
	if(strlen(user) > 10){
		return 0;
	}
	for(int i = 1;i<strlen(user);i++){
		chaine[i] = user[i] ^ 42;
		if(chaine[i] < 65 || chaine[i] > 90){
			chaine[i] = 'Y';
		}
		else{
			chaine[i] = toupper(chaine[i]);
		}
	}
	chaine[4] = '-';
	chaine[9] = '-';
	if(strcmp(chaine,key) == 0){
		return 1;
	}
	else{
		return 0;
	}
}
int main(int argc, char *argv[]){
	char username[20];
	char key[15];
	printf("[+] Username : ");
	scanf("%s",&username);
	printf("[+] Key for %s : ",&username);
	scanf("%14s",&key);
	if(key[4]!='-' || key[9]!='-' || strlen(key)!=14){
		printf("Invalid key format (AAAA-BBBB-CCCC)\n");
		return 0;
	}
	if(auth(username, key)){
		printf("Bravo, tu peux utiliser cette clef pour valider le challenge\n");
	}
	else{
		printf("Perdu :(\n");
	}
	return 0;
}