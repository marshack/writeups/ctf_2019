; source : crackme01.asm    
; Assemble:  nasm -f elf64 -l crackme01.lst  crackme01.asm
; Link:      gcc -m64 -no-pie -o crackme01  crackme01.o
; Run:       ./crackme01 <flag>

; fonctions de la libc qui doivent etre utilisees
extern printf             
extern atoi

section .rodata


banner: db 10 
        db "   __  __                        _    _            _",10    
        db "  |  \/  |                 ____ | |  | |          | |",10   
        db "  | \  / | __ _ _ __ ___  / __ \| |__| | __ _  ___| | __",10
        db "  | |\/| |/ _` | '__/ __|/ / _` |  __  |/ _` |/ __| |/ /",10
        db "  | |  | | (_| | |  \__ \ | (_| | |  | | (_| | (__|   < ",10
        db "  |_|  |_|\__,_|_|  |___/\ \__,_|_|  |_|\__,_|\___|_|\_\",10
        db "                          \____/",10                         
        db 0

fmt3: db "%s", 10, 0           ; The printf format, "\n",'0'

help: db "Help :",10
      db "    crackme01 <password>",10,0 


msg1: db "   Congratulations !",10
      db "   Validation : MARS{<password>}", 0   ; C string needs 0
fmt1: db "%s", 10,10, 0        ; The printf format, "\n",'0'

msg2: db "Failed", 0           ; C string needs 0
fmt2: db "%s", 10,10, 0           ; The printf format, "\n",'0'

section .data                  ; Data section, initialized variables

global main                    
section .text                  ; Code section.

main:                          ; the standard gcc entry point
                               ; the program label for the entry point
push rbp                       ; set up stack frame, must be alligned


; ---- verifie l'existence d'un paramêtre sur la ligne de commande    ---------


push rdi
push rsi
mov rsi,banner                 ; affiche la banner asciiart 
mov rdi,fmt3             
xor rax,rax                 
call printf                    ; appelle de la fonction c printf


pop rsi
pop rdi

cmp rdi,$2                     ; rdi => argc ,  rsi : argv
je continu                

mov rsi,help                   ; affiche l aide 
mov rdi,fmt3                
xor rax,rax                 
call printf                    ; appelle de la fonction c printf
jmp fin

continu:
mov rsi,qword[rsi+8]           ; [rsi+8] pointeur sur argument 1 => le password

mov rdi,rsi                    ; pointeur sur argument 1
call atoi                      ; fonction : conversion string en nombre 

mov rsi,0xdeadbeef             ; 0xdeadbeef = 3735928559 en décimal
cmp rax,rsi

jne badpassword 

goodpassword:
mov rdi,fmt1                
mov rsi,msg1                   ; affiche : congratulations 
xor rax,rax                 
;call [printf wrt ..got]       ; appelle de la fonction c printf
call printf                    ; appelle de la fonction c printf
jmp fin

badpassword: 
 
mov rdi,fmt2
mov rsi,msg2                   ; affiche : Failed 
xor rax,rax                
;call [printf wrt ..got]       ; appelle de la fonction c printf
call printf                    ; appelle de la fonction c printf

fin:
pop rbp                        ; restaure la pile
xor rax,rax                    ; normale, pas d'erreur
ret                            ; return
