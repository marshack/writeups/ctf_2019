; source : reverse03.asm
; Assemble:  nasm -f elf64 -l reverse03.lst  reverse03.asm
; Link:  gcc -s -m64  -o reverse03  reverse03.o
; Run:  ./reverse03 <flag>
   
; Declare needed C  functions


extern printf             ; the C function, to be called
extern signal
extern ptrace
extern fork
extern sleep
extern waitpid
extern exit

section .rodata    ; read only section

section .data      ; Data section, initialized variables

; flag = "Don_t_us3_p1ntctf_pl3as3_"
flagencode: db 0x63,0x48,0x1e,0x4b,0x0d,0x56,0x10,0x4c,0x16,0x0c,0x4a,0x1d,0x05,0x50,0x0b,0x41,0x1a,0x57,0x11,0x46,0x15,0x0f,0x47,0x1d,0x07,0x41 

msg2: db "int3",0x0a,0  ; C string needs 0
fmt2: db "%s", 10, 0       ; The printf format, "\n",'0'


msg3: db "le debug est interdit",0x0a, 0  ; C string needs 0
fmt3: db "%s", 10, 0       ; The printf format, "\n",'0'

msg4: db "congratulations",0x0a, 0  ; C string needs 0
fmt4: db "%s", 16, 0       ; The printf format, "\n",'0'

msg5: db "failed",0xa, 0  ; C string needs 0
fmt5: db "%s", 7, 0       ; The printf format, "\n",'0'

;fmt6: db "%x",0           ; format hexadecimal 



align 8
var01: dq 0x0
var02: dq 0x0
var03: dq 0x0
var04: dq 0x0
var05: dq 0X0
var06: dq 0x0
var07: dq 0x0

default rel
global main                    
section .text             ; Code section.


handler:


mov rsi,qword [var01] 
xor rbx,rbx      ;  compteur
xor rdi,rdi      ;  rdi mis a 0

mov rcx,flagencode


boucle :
mov rax,rsi      ;    rsi pointeur sur chaine saisi  
add rax,rbx      ;    rbx => index

; obfuscation
jmp label01 + 2
label01:
db 0xc6,0x06

movzx rax, byte[rax]   ;    rax  pointeur sur chaine saisi
sub rax,0x19

mov rcx,flagencode
add rcx,rbx

; obfuscation
jmp label02 + 2
label02:
db 0xc6,0x06


mov dil,byte[rcx]
xor al,dil
inc rcx
mov dil,byte[rcx]

; obfuscation
jmp label03 + 2
label03:
db 0xc6,0x06

cmp al,dil
setnz [var06]
mov rdi,[var06]
add [var07],rdi

inc rbx
cmp rbx,0x19
jne boucle  


mov rax,[var07]
test rax,rax
jne bad

good:

mov rdi,fmt4    ; congratulations
mov rsi,msg4
mov rax,0                 ; or can be  xor  rax,rax
call [printf wrt ..got]
ret

bad: 
mov rdi,fmt5    ;  failed
mov rsi,msg5
mov rax,0                 ; or can be  xor  rax,rax
call [printf wrt ..got]
ret



main:                     ; the standard gcc entry point
                          ; the program label for the entry point
push rbp                  ; set up stack frame, must be alligned

cmp rdi,$2
jne fin



mov rbx,qword[rsi+8]       ; [rsi+8] pointeur sur arguement 1
mov qword [var01],rbx


call [fork wrt ..got]
mov qword [var05],rax
cmp eax,0x0
jne parent

mov ecx,0x0
mov edx,0x1
mov esi,0x0
mov edi,0x0
mov eax,0x0
call [ptrace wrt ..got]
test rax,rax
jne noptrace
mov ebx,0x2
noptrace:
mov ecx,0x0
mov edx,0x1
mov esi,0x0
mov edi,0x0
mov eax,0x0
call [ptrace wrt ..got]
cmp rax,-1
jne antiunptrace
add ebx,2
antiunptrace:
cmp ebx,4
jne fin 


db 0xcc
mov rsi,handler
mov rdi,0x5
call [signal wrt ..got]

db 0xcc

mov edi,0x0
call [exit wrt ..got]


fin:
pop rbp                   ; restore stack

mov rax,0                 ; normal, no error, return value
ret                       ; return

parent:
mov qword[var02],0x0  ; [rbp-0xc]
mov qword[var03],0x0  ; [rbp-0x10]

waitsignal:
mov rcx,var03     ;[rbp-0x10]
mov eax,[var05]  ;  [rbp-0x8]
mov edx,0x2
mov rsi,rcx
mov edi,eax
call [waitpid wrt ..got]
mov qword[var02],rax
mov rax,qword[var03]
movzx eax,al
cmp eax,0x7f
jne testsignalexit
mov rax,qword[var03]  ; [rbp-0x10]
sar eax,0x8
movzx eax,al
cmp eax,0x5
jne testsignalexit


mov rax,qword[var05]
mov ecx,0x0
mov edx,0x0
mov esi,eax
mov edi,0x11
mov eax,0x0
call [ptrace wrt ..got]

testsignalexit:
mov rax,qword[var03]
and eax,0x7F
test eax,eax
je quittesignal
jmp waitsignal

quittesignal:

pop rbp
mov rax,0
ret