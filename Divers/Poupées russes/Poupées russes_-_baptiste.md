# Poupées russes

Consignes :

```
Résolvez les différentes étapes de ce challenge, le flag pour valider l'épreuve sera le mot de passe de la dernière étape.
```

Pièce jointe :

```
logo.png
```

Serveur :

```
CTFD
```

Points attribués :

```
200
```

Flag : 

```
MARS{H0pp3R}
```
