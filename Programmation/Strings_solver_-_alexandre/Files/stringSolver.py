#!/usr/bin/python2.7

import requests, pytesseract
from PIL import Image
from base64 import b64decode
from io import BytesIO

url = "http://localhost:8080"

r = requests.get(url)

cookie = r.cookies

while (r.text.find('Level') > -1):
	lvl = r.text[r.text.find('Level '):]
	lvl = lvl[:lvl.find('</')]
	print lvl

	im1data = r.text[r.text.find('base64')+7:]
	im1data = im1data[:im1data.find('">')]
	im1data = b64decode(im1data)
	im1 = Image.open(BytesIO(im1data))

	im2data = r.text[r.text.rfind('base64')+7:]
	im2data = im2data[:im2data.find('">')]
	im2data = b64decode(im2data)
	im2 = Image.open(BytesIO(im2data))

	im1.show()

	str1 = pytesseract.image_to_string(im1)
	print "    " + str1

	im2.show()

	str2 = pytesseract.image_to_string(im2)
	print "    " + str2

	rtnstr = str1 + str2
	print "    " + rtnstr

	data = {"string": rtnstr}

	r = requests.post(url, cookies=cookie, data=data)

flag = r.text[r.text.find('h2>')+3:]
flag = flag[:flag.find('</h2')]
print flag
